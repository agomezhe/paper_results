# Dynamic and Static Object Detection Considering Fusion Regions and Point-wise Features

**detection-results:** You can find all the files(*.txt) with the detections obtained with our algorithm.

**ground-truth:** You can find all the files(*.txt) with the objects labeled in the KITTI MOD dataset for the experiment.
	
**images-optional:** In this folder, it is possible to find all the images(*.png) used in the experiment.

**results:** This folder has a complete description of the results obtained with the mAP tool.

**main_val.py:** Python script used to validate the fusion process considering the longitudinal distance of thirty meters.


**Related links:**

**DEMO:** https://youtu.be/Rd-0B0--mlc

**MODNet:** https://webdocs.cs.ualberta.ca/~vis/kittimoseg/

**mAP:** https://github.com/Cartucho/mAP
